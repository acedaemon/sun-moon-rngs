class SFMT(object):

    def __init__(self, seed):
        self.pos1 = 122
        self.sl1 = 18
        self.sr1 = 11
        self.mask = [ 0xdfffffef, 0xddfecb7f, 0xbffaffff, 0xbffffff6 ]
        self.parity = [ 0x00000001, 0x00000000, 0x00000000, 0x13c9e684 ]
        self.n32 = ((19937>>7) + 1 ) << 2

        self.sfmt = [None]*self.n32

        self.sfmt[0] = seed & 0xffffffff
        for i in range(1,self.n32):
            self.sfmt[i] = 1812433253 * (self.sfmt[i-1] ^ (self.sfmt[i-1] >> 30)) + i
            self.sfmt[i] &= 0xffffffff
            
        self._period_cert()
        self.idx = self.n32


    def _period_cert(self):
        inner = 0
        for i in range(4):
            inner ^= self.sfmt[i] & self.parity[i]
        for i in range(5):
            inner ^= inner >> (16 >> i)
        inner &= 1
        if inner == 1:
            return
        for i in range(4):
            work = 1
            for j in range(32):
                if (work & self.parity[i]) != 0:
                    self.sfmt[i] ^= work
                    return
                work <<= 1


    def _step(self):
        p = self.sfmt
        a = 0
        b = 488 #self.pos1*4
        c = 616 #(self.n - 2)*4
        d = 620 #(self.n - 1)*4

        while a < self.n32:
            p[a+3] ^= (p[a+3] << 8)^(p[a+2] >> 24) ^ (p[c+3] >> 8) ^ ((p[b+3] >> self.sr1) & self.mask[3]) ^ (p[d+3] << self.sl1)
            p[a+3] &= 0xffffffff

            p[a+2] ^= (p[a+2] << 8)^(p[a+1] >> 24) ^ (p[c+3] << 24)^(p[c+2] >> 8) ^ ((p[b+2] >> self.sr1) & self.mask[2]) ^ (p[d+2] << self.sl1)
            p[a+2] &= 0xffffffff

            p[a+1] ^= (p[a+1] << 8)^(p[a+0] >> 24) ^ (p[c+2] << 24)^(p[c+1] >> 8) ^ ((p[b+1] >> self.sr1) & self.mask[1]) ^ (p[d+1] << self.sl1)
            p[a+1] &= 0xffffffff

            p[a+0] ^= (p[a+0] << 8)^ (p[c+1] << 24)^(p[c+0] >> 8) ^ ((p[b+0] >> self.sr1) & self.mask[0]) ^ (p[d+0] << self.sl1)
            p[a+0] &= 0xffffffff

            c, d, a = d, a, a+4
            b = b+4 if b+4 < self.n32 else 0


    def nextState(self):
        self.idx += 2
        if self.idx >= self.n32:
            self._step()
            self.idx = 0

    def nextStateAsInt(self, limit):
        self.nextState()
        return self.getStateAsInt(limit)

    def getStateAsInt(self, limit):
        val = self.sfmt[self.idx]
        val |= self.sfmt[self.idx + 1] << 32
        return val % limit
