#!/usr/bin/env python3

from ast import literal_eval as l_eval
import fastmt
from pokemons import Wild, Target, natures, ratios, types, parseInput, get_sv, get_hpower
from copy import deepcopy as dc

# Types of encounters
encounters = ["WALK", "HONEY", "FISH", "NONE"]

# Contain special encounter information for named access and succinct definition
class SpecialPkmn(object):
    def __init__(self, level, models, ivs, shinylock, delay, cry, honey, ubr, etype):
        self.lvl = level
        self.models = models
        self.ivs = ivs
        self.shinylock = shinylock
        self.delay = delay
        self.cry = cry
        self.honey = honey
        self.ubr = ubr
        self.etype = etype

special_pkmn = {
    "NONE"          : SpecialPkmn(0, 0, 0, False, 0, 0, 0, 0, None),

    "TAPUKOKO"	    : SpecialPkmn(60, 0, 3, True, 0, 0, 0, 0, "INTERACT"),
    "TAPULELE"	    : SpecialPkmn(60, 0, 3, True, 0, 0, 0, 0, "INTERACT"),
    "TAPUBULU"	    : SpecialPkmn(60, 0, 3, True, 0, 0, 0, 0, "INTERACT"),
    "TAPUFINI"	    : SpecialPkmn(60, 1, 3, True, 0, 0, 0, 0, "INTERACT"),

    "SOLGALEO"	    : SpecialPkmn(55, 2, 3, True, 288, 77, 0, 0, "INTERACT"),
    "LUNALA"	    : SpecialPkmn(55, 2, 3, True, 282, 74, 0, 0, "INTERACT"),

    "COSMOG"	    : SpecialPkmn( 5, 3, 3, True,  34, 0, 0, 0, "GIFT"),

    "TYPENULL"	    : SpecialPkmn(40, 8, 3, False, 34, 0, 0, 0, "GIFT"),
    "MAGEARNA"	    : SpecialPkmn(50, 6, 3, True,  32, 0, 0, 0, "GIFT"),
    "ZYGARGE"	    : SpecialPkmn(50, 3, 3, True,  32, 0, 0, 0, "GIFT"),

    "AERODACTYL"	: SpecialPkmn(40, 3, 0, False, 34, 0, 0, 0, "GIFT"),
    "PORYGON"	    : SpecialPkmn(30, 4, 0, False, 34, 0, 0, 0, "GIFT"),
    "FOSSIL"	    : SpecialPkmn(15, 1, 0, False, 40, 0, 0, 0, "GIFT"),

    "CRABRAWLER"    : SpecialPkmn( 0, 1, 0, False,  4, 0, 0, 0, "UNKNOWN"),

    "NIHILEGO"	    : SpecialPkmn(55, 0, 3, True, 0, 0, 13, 80, None), 
    "BUZZWOLE"	    : SpecialPkmn(65, 0, 3, True, 0, 0,  5, 30, None),
    "PHEROMOSA"	    : SpecialPkmn(60, 0, 3, True, 0, 0,  5, 50, None),
    "XURKITREE"	    : SpecialPkmn(65, 0, 3, True, 0, 0,  7, 15, None),
    "CELESTEELA"	: SpecialPkmn(65, 0, 3, True, 0, 0, 26, 30, None),
    "KARTANA"	    : SpecialPkmn(60, 0, 3, True, 0, 0, 26, 30, None),
    "GUZZLORD"	    : SpecialPkmn(70, 0, 3, True, 0, 0,  1, 80, None),
    "NECROZMA"	    : SpecialPkmn(75, 0, 3, True, 0, 0,  1,  5, None) }

# Determine encounter slot for wild pokemon
def get_sid(roll):
    if roll < 40:
        sid = int(roll/20) + 1
    elif roll < 90:
        sid = int(roll/10) - 1
    elif roll < 95:
        sid = 8
    elif roll < 99:
        sid = 9
    else:
        sid = 10
    return sid


def readConfigFile(filename):
    with open(filename) as cfgfile:
        config = cfgfile.readlines()
        params = {}

        # Read target parameters
        skip = 3
        # Read IVs as range of integers 0-31
        ivs = []
        for i in range(6):
            try:
                iv_range = l_eval(parseInput(config[skip+i].split(':')[1]))
            except Exception:
                return None, "Invalid IV range for target"
            if type(iv_range) != list or len(iv_range) != 2:
                return None, "Ivalid IV range format"
            for iv in iv_range:
                if iv < 0 or iv > 31:
                    return None, "IVs out of 0-31 range"
            ivs.append(iv_range)
        # Ability slot should be one of Anything, 1, 2, HA
        ability = parseInput(config[skip+6].split(':')[1]).upper()
        if ability not in ["ANYTHING", "1", "2", "HA"]:
            return None, "Invalid ability for target"
        ability = None if ability == "ANYTHING" else ability
        # Nature should be one of the valid natures or "Anything"
        nature = parseInput(config[skip+7].split(':')[1]).upper()
        if nature not in ["ANYTHING"]+natures:
            return None, "Invalid nature for target"
        nature = None if nature == "ANYTHING" else nature
        # Gender should be one of Anything, M, F, Genderless
        gender = parseInput(config[skip+8].split(':')[1]).upper()
        if gender not in ["ANYTHING", "M", "F", "GENDERLESS"]:
            return None, "Invalid gender for target"
        gender = None if gender == "ANYTHING" else gender
        # Hidden power should be a valid type
        hpower = parseInput(config[skip+9].split(':')[1]).upper()
        if hpower not in ["ANYTHING"]+types:
            return None, "Invalid hidden power type"
        hpower = None if hpower == "ANYTHING" else hpower
        # Shiny should be one of Anything, Y, N
        shiny = parseInput(config[skip+10].split(':')[1]).upper()
        if shiny not in ["ANYTHING", "Y", "N"]:
            return None, "Invalid shiny parameter for child"
        shiny = True if shiny == "Y" else False if shiny == "N" else None
        # Build the target pokemon, ignoring pid, psv, and #rolls
        try:
            slots = l_eval(parseInput(config[skip+11].split(':')[1]))
        except Exception:
            return None, "Invalid list of slots"
        if type(slots) != list:
            return None, "Invalid list of slots formatting"

        skip = 19
        # Read RNG seed
        try:
            seed = int(config[skip+0].split(':')[1], 16)
        except ValueError:
            return None, "Invalid seed status"
        params["seed"] = seed
        # Read TSV as integer between 0 and 4096
        try:
            tsv = int(config[skip+1].split(':')[1])
        except ValueError:
            return None, "Invalid TSV"
        if tsv < 0 or tsv > 4096:
            return None, "TSV must be between 0 and 4096"
        params["tsv"] = tsv
        # Read gender ratio
        ratio = parseInput(config[skip+2].split(':')[1]).upper()
        if ratio not in ratios:
            return None, "Invalid gender ratio"
        params["ratio"] = ratios[ratio]
        # Read shiny charm info
        charm = parseInput(config[skip+3].split(':')[1]).upper()
        if charm not in ["Y", "N"]:
            return None, "Invalid Shiny Charm parameter"
        params["charm"] = True if charm == "Y" else False
        # Read synchronise info
        sync = parseInput(config[skip+4].split(':')[1]).upper()
        if sync not in ["NONE"]+natures:
            return None, "Invalid Synchronise parameter"
        params["sync"] = None if sync == "NONE" else sync
        # Read legendary info
        legend = parseInput(config[skip+5].split(':')[1]).upper()
        if legend not in special_pkmn:
            return None, "Invalid Lengendary parameter"
        params["legend"] = special_pkmn[legend]
        # Read gift info
        enc = parseInput(config[skip+6].split(':')[1]).upper()
        if enc not in encounters:
            return None, "Invalid Encounter Type parameter"
        params["etype"] = "NONE" if params["legend"].etype else enc
        # Read level range
        try:
            lvl_range = l_eval(parseInput(config[skip+7].split(':')[1]))
        except Exception:
            return None, "Invalid level range for target"
        if type(lvl_range) != list or len(lvl_range) != 2:
            return None, "Ivalid level range format"
        if lvl_range[0] < 1 or lvl_range[1] > 100:
            return None, "Levels out of valid range"
        params["levels"] = lvl_range
        # Read number of desired results
        try:
            params["nresults"] = int(config[skip+8].split(':')[1])
        except ValueError:
            return None, "Invalid number of results to be shown"

        params["target"] = Target(ivs, ability, nature, gender, 
                                  shiny, hpower, slots, params["legend"].ubr)
        return params, None
        
def timeElapse(sfmt, models, n):
    rolls = 0
    for j in range(n):
        for i in range( len(models) ):
            if models[i] > 1:
                models[i] -= 1
            elif models[i] < 0:
                models[i] += 1
                if models[i] == 0:
                    rolls += 1
                    models[i] = 36 if sfmt.nextStateAsInt(3) == 0 else 30
            else:
                rolls += 1
                models[i] = -5 if sfmt.nextStateAsInt(1<<7) == 0 else models[i]
        # every other call on route 17 steps RNG twice here
    return rolls

def initModels(n_models):
    # track blinking status of models
    return [0]*n_models

def makeWild(sfmt, models, pkmn, etype, snature, levels, charm, ratio, tsv):

    if etype == "HONEY":
        # Always include the time needed to open the bag
        timeElapse(sfmt, models, 6)
        # Reset model status while not they are not drawn
        models = initModels( 1 + pkmn.models )
        # Route 17 steps RNG twice, in addition to the models possibly blinking
        timeElapse(sfmt, models, 1)
        [ sfmt.nextState() for i in range(pkmn.honey) ]
    else:
        timeElapse(sfmt, models, 2)

    delay = 93 if not pkmn.delay else pkmn.delay >> 1
    if pkmn.cry:
        rolls = timeElapse(sfmt, models, delay - pkmn.cry - 19)
        # Two models disappear during screen fade
        if len( models ) == 7:
            models = models[:3] + models[5:]
        rolls += timeElapse(sfmt, models, 19)
        sfmt.nextState()
        rolls += timeElapse(sfmt, models, pkmn.cry)
    else:
        rolls = timeElapse(sfmt, models, delay)

    # Ultra Beast using honey
    if etype == "HONEY" and pkmn.ubr:
        ub = sfmt.nextStateAsInt(100) < pkmn.ubr
    else:
        ub = None

    # While fishing, encounter is checked before synchronize activating
    if etype == "FISH":
        encounter = sfmt.nextStateAsInt(100)
        timeElapse(sfmt, models, 12)
    else:
        encounter = None

    # Check whether or not synchronize activates
    if ub:
        # Always blink
        timeElapse(sfmt, models, 7)

    if pkmn.etype == "GIFT":
        sync = snature is not None
    else:
        sync = sfmt.nextStateAsInt(100) >= 50

    # Roll for an encounter, unless crabrawler,
    # or SOS which is unsupported
    if etype == "WALK":
        encounter = sfmt.nextStateAsInt(100)
        if pkmn.ubr:
            ub = sfmt.nextStateAsInt(100) < pkmn.ubr
        if ub:
            sfmt.nextState()
            sync = sfmt.nextStateAsInt(100) >= 50

    if ub or pkmn.etype == "INTERACT":
        timeElapse(sfmt, models, 3)

    # Roll for encounter slot and level, if needed
    if etype != "NONE" and not ub:
        slot = get_sid(sfmt.nextStateAsInt(100))
        level = sfmt.nextStateAsInt(levels[1]-levels[0]+1) + levels[0]
        sfmt.nextState()
    else:
        slot = None
        level = pkmn.lvl

    # Unknown, skipped for guaranteed synchronization and SOS
    something = 60 if pkmn.etype != "GIFT" else 0
    [ sfmt.nextState() for i in range(something) ]

    # Roll Encryption Constant
    encrypt = sfmt.nextStateAsInt(1<<32)

    # Roll PID and check shininess
    rerolls = 3 if charm and not pkmn.lvl else 1
    for i in range(rerolls):
        pid = sfmt.nextStateAsInt(1<<32)
        psv = get_sv(pid)
        shiny = "Yes" if psv == tsv else "No"
        if shiny == "Yes":
            break
    # Remove shiny if shiny locked
    if pkmn.shinylock and shiny == "Yes":
        pid ^= 0x10000000
        psv ^= 0x100
        shiny = "No"

    # Generate IVs
    ivs = [None]*6
    while (ivs.count(31) < pkmn.ivs):
        ivs[ sfmt.nextStateAsInt(6) ] = 31
    # Roll random IVs
    for stat in range(6):
        if ivs[stat] is None:
            ivs[stat] = sfmt.nextStateAsInt(32)
    hpower = get_hpower(ivs)

    # Roll for ability if relevant
    if not pkmn.lvl or pkmn.etype == "GIFT":
        ability = sfmt.nextStateAsInt(2) + 1
    else:
        ability = "-"

    # Roll for nature
    nature = natures[sfmt.nextStateAsInt(25)]
    if sync and snature:
        nature = snature
    elif not sync:
        sfmt.nextState()
       

    # Roll for or set gender
    if ratio == 255:
        gender = "GENDERLESS"
    elif ratio == 254:
        gender = "F"
    elif not ratio:
        gender = "M"
    else:
        gender = "F" if sfmt.getStateAsInt(252)+1 < ratio else "M"
        sfmt.nextState()

    # Generate item slot
    if not pkmn.lvl:
        item = sfmt.getStateAsInt(100)
        item = "50%" if item < 50 else "5%" if item < 55 else "100%"
        item += " slot"
        sfmt.nextState()
    else:
        item = "-"

    # Build pokemon and return
    return Wild(ivs, ability, nature, gender, 
                hpower, shiny, level, item,
                ub, slot, rolls, None)





def main():
    # Read configuration
    try:
        params, msg = readConfigFile("wild.txt")
    except Exception:
        msg = "Unknown error occured..."
    if msg is not None:
        with open("output.txt", 'w') as res:
            res.write("There was an error processing the config file:\n")
            res.write("ERROR: {}\n".format(msg))
        return

    results = []
    sfmt = fastmt.SFMT(params["seed"])
    # Here we skip 418 calls to the RNG, which occur before opening a file
    count = 418
    [ sfmt.nextState() for i in range(count) ]

    models = initModels( 1 + params["legend"].models )

    while not results or len(results) < params["nresults"]:

        mon = makeWild( dc(sfmt), dc(models), params["legend"],
                params["etype"], params["sync"], params["levels"], 
                params["charm"], params["ratio"], params["tsv"])

        if params["target"].matches(mon):
            frame = count
            frame += 0 if params["etype"] == "HONEY" else mon.rolls
            results.append( (frame, mon) )

        sfmt.nextState()
        count += 1 + timeElapse( sfmt, models, 1 )
        if not count % 50000:
            print("{} checked".format(count))

    print("Found {} results".format(len(results)))
    with open("output.txt", 'w') as res:
        for frame, mon in results:
            res.write(str(mon))
            tmp = "Open bag" if params["etype"] == "HONEY" else "Close LAST textbox"
            res.write( "\n{} on frame: {}\n".format(tmp, frame) )

if __name__ == "__main__":
    main()
