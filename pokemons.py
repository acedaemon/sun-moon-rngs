# Natures in game order
natures = ["HARDY", "LONELY", "BRAVE", "ADAMANT", "NAUGHTY", "BOLD", "DOCILE",
           "RELAXED", "IMPISH", "LAX", "TIMID", "HASTY", "SERIOUS", "JOLLY",
           "NAIVE", "MODEST", "MILD", "QUIET", "BASHFUL", "RASH", "CALM",
           "GENTLE", "SASSY", "CAREFUL", "QUIRKY"]

# Gender ratio rolls
ratios = {"GENDERLESS": 255, "FEMALE": 254, "MALE": 0, "1-1": 126,
          "1-3": 189, "3-1": 63, "1-7": 220, "7-1": 32}

# Hidden power types
types = ["FIGHTING", "FLYING", "POISON", "GROUND", "ROCK", "BUG", "GHOST",
         "STEEL", "FIRE", "WATER", "GRASS", "ELECTRIC", "PSYCHIC", "ICE",
         "DRAGON", "DARK"]

# Clean input file line from excess characters
def parseInput(inp):
    res = inp
    badcharacters = [' ', '\n', '\r']
    for ch in badcharacters:
        res = "".join(res.split(ch))
    return res


# Checks whether a PID is shiny based on TSV
def get_sv(pid):
    pid_high = (pid >> 16)
    pid_low = (pid & 0xFFFF)
    return ((pid_high ^ pid_low) >> 4)


# Determine hidden power type
def get_hpower(ivs):
    bits = [str(iv[1] % 2) for iv in ivs][::-1]
    # Swap speed with spatk and spdef
    bits[0], bits[1], bits[2] = bits[1], bits[2], bits[0]
    hpower = int(int("".join(bits), 2) * 15 / 63)
    return types[hpower]

def get_hpowerpower(ivs):
    bits = [str((iv & 2)>>1) for iv in ivs][::-1]
    bits[0], bits[1], bits[2] = bits[1], bits[2], bits[0]
    hpp = int(int("".join(bits), 2) * 40 / 63) + 30
    return hpp

class Pokemon(object):
    
    def __init__(self, ivs, ability, nature, gender):
        self.ivs = ivs
        self.ability = ability
        self.nature = nature
        self.gender = gender


class Parent(Pokemon):

    def __init__(self, ivs, item, ability, nature, gender, ditto):
        super(Parent, self).__init__(ivs, ability, nature, gender)
        self.item = item
        self.ditto = ditto


class Egg(Pokemon):

    def __init__(self, seeds, ivs, ability, nature, gender, pid, ball, rolls,
                 esv, shiny, hpower):
        super(Egg, self).__init__(ivs, ability, nature, gender)
        self.ball = ball
        self.seeds = seeds
        self.pid = pid
        self.rolls = rolls
        self.esv = esv
        self.shiny = shiny
        self.hpower = hpower

    def __str__(self):
        result = "IV Spread: "
        for iv in self.ivs:
            result += "({}, {}) / ".format(iv[0], iv[1])
        result = result[:-3] + '\n'
        result += "Ability: {}\n".format(self.ability)
        result += "Nature: {}\n".format(self.nature)
        result += "Gender: {}\n".format(self.gender)
        result += "Ball Inherited: {}\n".format(self.ball)
        result += "Hidden Power Type: {}\n".format(self.hpower)
        result += "PID: {}\n".format(hex(self.pid)[2:])
        result += "ESV: {}\n".format(self.esv)
        result += "Shiny: {}\n".format("Yes if traded" if self.shiny == "P"
                                       else self.shiny)
        result += "Seed to hatch: "
        for seed_step in self.seeds[0][::-1]:
            result += "{} ".format(hex(seed_step)[2:])
        result = result[:-1] + '\n'
        result += "Seed after hatching: "
        for seed_step in self.seeds[1][::-1]:
            result += "{} ".format(hex(seed_step)[2:])
        result = result[:-1] + '\n'
        return result


class Child(Pokemon):

    def __init__(self, ivsRange, ability, nature, gender, ball, hpower, shiny):
        super(Child, self).__init__(ivsRange, ability, nature, gender)
        self.ivsRange = ivsRange
        self.ball = ball
        self.hpower = hpower
        self.shiny = shiny

    def matches(self, egg):
        # Check for IVs withing range
        for i in range(6):
            eggIV = egg.ivs[i][1]
            childIVRange = self.ivsRange[i]
            if eggIV < childIVRange[0] or eggIV > childIVRange[1]:
                return False
        # Check for ability, nature, gender ball, hpower, shiny
        if self.ability is not None and egg.ability != self.ability:
            return False
        if self.nature is not None and egg.nature != self.nature:
            return False
        if self.gender is not None and egg.gender != self.gender:
            return False
        if self.ball is not None and egg.ball != self.ball:
            return False
        if self.hpower is not None and egg.hpower != self.hpower:
            return False
        if self.shiny and egg.shiny not in ["Yes", "P"]:
            return False
        elif self.shiny is not None and not self.shiny and egg.shiny != "No":
            return False
        return True


class Wild(Pokemon):

    def __init__(self, ivs, ability, nature, gender, 
                 hpower, shiny, level, item,
                 ub, slot, rolls, debug):
        super(Wild, self).__init__(ivs, ability, nature, gender)
        self.hpower = hpower
        self.shiny = shiny
        self.level = level
        self.item = item

        self.ub = ub
        self.slot = slot
        self.rolls = rolls
        self.debug = debug

    def __str__(self):
        result = "IV Spread: {}\n".format(self.ivs)
        result += "Hidden Power: {}\n".format(self.hpower)
        result += "Ability: {}\n".format(self.ability)
        result += "Nature: {}\n".format(self.nature)
        result += "Gender: {}\n".format(self.gender)
        result += "Shiny: {}\n".format(self.shiny)
        result += "Level: {}\n".format(self.level)
        result += "Item Slot: {}\n".format(self.item)
        if self.debug:
            result += "Rolls: {}\n{}\n".format(self.rolls, self.debug)

        return result

class Target(Pokemon):

    def __init__(self, ivsRange, ability, nature, gender,
                 shiny, hpower, slots, ub):
        super(Target, self).__init__(ivsRange, ability, nature, gender)
        self.shiny = shiny
        self.hpower = hpower
        self.slots = slots
        self.ub = ub

    def matches(self, wild):
        for i in range(6):
            wildIV = wild.ivs[i]
            ivrange = self.ivs[i]
            if wildIV < ivrange[0] or wildIV > ivrange[1]:
                return False

        if self.ability and wild.ability != self.ability:
            return False
        if self.nature and wild.nature != self.nature:
            return False
        if self.gender and wild.gender != self.gender:
            return False
        if self.shiny and wild.shiny != self.shiny:
            return False
        if self.hpower and wild.hpower[0] != self.hpower:
            return False

        if self.ub and not wild.ub:
            return False
        if len(self.slots) > 0 and wild.slot not in self.slots:
            return False

        return True
